import React, { useState } from 'react';
import axios from 'axios';

export default function SearchForm() {
  const [query, setQuery] = useState('');
  const [response, setResponse] = useState('');

  const handleSubmit = async (event) => {
    event.preventDefault();
    const url = `https://www.google.com/search?q=${query}`;
    try {
      const res = await axios.get(url);
      setResponse(res.data);
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <label>
        Query:
        <input type="text" value={query} onChange={(e) => setQuery(e.target.value)} />
      </label>
      <button type="submit">Search</button>
      <pre>{response}</pre>
    </form>
  );
}
