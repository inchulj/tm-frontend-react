import React, { useState } from 'react';

function PostApi() {
  const [formData, setFormData] = useState('');

  const handleFormSubmit = () => {
    // Perform your POST API request with the form data.
    // Replace this with your actual API request logic.
    console.log('Sending data to the POST API:', formData);
  };

  return (
    <div>
      <h2>POST API Page</h2>
      <form>
        <input
          type="text"
          placeholder="Form Data"
          value={formData}
          onChange={(e) => setFormData(e.target.value)}
        />
        <button onClick={handleFormSubmit}>Submit</button>
      </form>
    </div>
  );
}

export default PostApi;
