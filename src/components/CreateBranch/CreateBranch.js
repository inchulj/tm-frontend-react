import React, { useState } from 'react';
import axios from 'axios';

export default function CreateBranch() {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [response, setResponse] = useState('');

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const response = await axios.post(
        'https://tms-env.eba-ydgu2qti.us-east-1.elasticbeanstalk.com/tourism/api/v1/branch/add-places',
        { name, email }
      );
      setResponse(response.data);
    } catch (error) {
      setResponse(error.message);
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <label>
        Name:
        <input type="text" value={name} onChange={(e) => setName(e.target.value)} />
      </label>
      <label>
        Email:
        <input type="email" value={email} onChange={(e) => setEmail(e.target.value)} />
      </label>
      <button type="submit">Submit</button>
      {response && <p>{response}</p>}
    </form>
  );
}
