
import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';

function Dashboard({ authorized }) {
  const [searchParam, setSearchParam] = useState('');
  const [searchParam2, setSearchParam2] = useState('');
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [website, setWebsite] = useState('');
  const [contact, setContact] = useState('');
  const [datetime, setDatetime] = useState('');
  const [job, setJob] = useState('');

  if (!authorized) {
    // If the user is not authorized, redirect to the login page.
    return <Redirect to="/login" />;
  }

  const openGoogleInNewTab = () => {
    window.open(`http://www.localhost:8082/tourism/api/v1/admin/branchName/${searchParam}`, '_blank');
  };

  const handleFormSubmit = () => {
    // Perform the POST API request with name and email.
    const postData = {
      name,
      email, website, contact, datetime
    };

    // Replace 'localhost:8082/add' with your actual API endpoint.
    fetch('http://www.localhost:8082/tourism/api/v1/branch/add-places', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(postData),
    })
//      .then(response => response.json())
      .then(data => {
        window.open('Response from the POST API:', data);
        console.log('Response from the POST API:', data);
      })
      .catch(error => {
        console.error('Error:', error);
      });
  };

  const handleOnSubmit = () => {
    // Perform the POST API request with name and email.
    const data = {
      name,
      email, contact, website, datetime
    };
    alert("post data= "+ JSON.stringify(data));
  }

  return (
    <div>
      <h2>Dashboard</h2>
      <div>
        <h3>Search Branch</h3>
        <input
          type="text"
          placeholder="Branch Name"
          value={searchParam}
          onChange={(e) => setSearchParam(e.target.value)}
        />
        <button onClick={openGoogleInNewTab}>Search by Name</button><br />
        <input
          type="text"
          placeholder="Branch ID"
          value={searchParam2}
          onChange={(e) => setSearchParam2(e.target.value)}
        />
        <button >Search by ID</button>
      </div>

      <div>
        <h3>Create Branch</h3>
        <form name="postform" onSubmit={handleFormSubmit} target="_blank" >
          <input
            type="text"
            placeholder="Name"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
          <input
            type="text"
            placeholder="Email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
          <input type="text" placeholder="Website" name="website" value={website} onChange={(e) => setWebsite(e.target.value)}/>
          <input type="text" placeholder="Contact" name="contact" value={contact} onChange={(e) => setContact(e.target.value)}/>
          <input type="text" placeholder="DateTime" name="datetime" value={datetime} onChange={(e) => setDatetime(e.target.value)}/>
          <input type="text" placeholder="Job" name="job" value={job} onChange={(e) => setJob(e.target.value)}/>

          <input type="submit" value="Submit"/>
        </form>
      </div>
    </div>
  );
}

export default Dashboard;

