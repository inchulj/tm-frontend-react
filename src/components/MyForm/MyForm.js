import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';

export default function MyForm({authorized}) {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [response, setResponse] = useState('');

  // if (!authorized) {
  //   // If the user is not authorized, redirect to the login page.
  //   return <Redirect to="/login" />;
  // }

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = { name, email };
    const response = await fetch('http://www.localhost:8081/tourism/api/v1/branch/add-places', {
      method: 'POST',
    cache: 'no-cache',
    credentials: 'same-origin',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(data),
    });
    const responseData = await response.json();
    setResponse(responseData.message);
  };

  return (
    <form onSubmit={handleSubmit}>
      <label>
        Name:
        <input type="text" value={name} onChange={(e) => setName(e.target.value)} />
      </label>
      <label>
        Email:
        <input type="email" value={email} onChange={(e) => setEmail(e.target.value)} />
      </label>
      <button type="submit">Submit</button>
      {response && <p>{response}</p>}
    </form>
  );
}
