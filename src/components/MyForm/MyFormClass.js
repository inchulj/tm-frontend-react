import React, { Component } from 'react';

class MyForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      message: '',
      response: null,
      error: null,
    };
  }

  handleInputChange = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  }

  handleSubmit = (event) => {
    event.preventDefault();

    const data = {
      name: this.state.name,
      email: this.state.email,
      message: this.state.message,
    };

    alert("postdata= "+ JSON.stringify(data));
	
    fetch('http://localhost:8081/tourism/api/vi/branch/add-places', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((data) => {
        this.setState({ response: data, error: null });
      })
      .catch((error) => {
        this.setState({ response: null, error: error.message });
      });
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <input
            type="text"
            name="name"
            placeholder="Name"
            value={this.state.name}
            onChange={this.handleInputChange}
          />
          <input
            type="email"
            name="email"
            placeholder="Email"
            value={this.state.email}
            onChange={this.handleInputChange}
          />
          <textarea
            name="message"
            placeholder="Message"
            value={this.state.message}
            onChange={this.handleInputChange}
          />
          <button type="submit">Submit</button>
        </form>

        {this.state.response && (
          <div>
            <h2>Response from the API:</h2>
            <pre>{JSON.stringify(this.state.response, null, 2)}</pre>
          </div>
        )}

        {this.state.error && (
          <div>
            <h2>Error:</h2>
            <p>{this.state.error}</p>
          </div>
        )}
      </div>
    );
  }
}

export default MyForm;