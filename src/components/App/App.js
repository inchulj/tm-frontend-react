import React, { useState } from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import Login from '../Login/Login';
import Dashboard from '../Dashboard/Dashboard.js';
import MyForm from '../MyForm/MyForm';

function App() {
  const [authorized, setAuthorized] = useState(false);

  const handleLogin = (credentials) => {
    // Here, you would validate the user's credentials and set authorized to true if authorized.
    // For simplicity, we'll just set authorized to true.
    setAuthorized(true);
  };

  return (
    <Router>
      <div>
        <Switch>
          <Route path="/login" render={() => (authorized ? <Redirect to="/dashboard" /> : <Login onLogin={handleLogin} />)} />
          <Route path="/dashboard" render={() => <Dashboard authorized={authorized} />} />
          <Route path="/myform" render={() => <MyForm  authorized={authorized} />} />
          <Redirect from="/" to="/dashboard" />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
